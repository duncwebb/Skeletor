Skeletor
========

Skeletor is a tool for creating wizards that create skeleton file sets based on a JSON or YAML instruction file and a
set of template files. Skeletor wizards can be created to create bootstrap file sets for any project regardless of
language or platform.

The main package skeletor_core provides the main JSON / YAML processing logic. In addition, the skeletor_base package
contains a wizard example that can be used to create skeletons for new Skeletor wizards. This can be executed using::

	skeletor_cli --help

