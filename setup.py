from setuptools import setup, find_packages

setup(
    name='skeletor',
    version='1.0.0',
    packages=find_packages(),
    package_data={'': ["LICENSE.txt"], 'skeletor_base': ['source/*', 'skeletor_cli.yaml']},
    url='http://www.python.org',
    license='http://opensource.org/licenses/Apache-2.0',
    author='Duncan Webb',
    author_email='duncwebb@googlemail.com',
    scripts=['skeletor_base/skeletor_cli.py', ],
    description='Template based project project starting point builder.',
    long_description=open('README.rst').read(),
    install_requires=[
        "docopt",
        "jinja2",
        "pyyaml",
    ],
)
