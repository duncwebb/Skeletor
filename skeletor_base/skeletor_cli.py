#!/usr/bin/env  python
"""\
skeletor_cli.py

Usage:
  skeletor_cli.py new
  skeletor_cli.py (-h | --help)
  skeletor_cli.py --version

Options:
  -h --help     Show this screen.
  --version     Show version.

"""

from docopt import docopt
from skeletor_base import skeletor_cli_main

if __name__ == "__main__":
    skeletor_cli_main.run_wizard(docopt(__doc__, version='Make a new Skeletor wizard 1.0'))
