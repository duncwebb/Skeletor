import os
from skeletor_core import skeletor


def run_wizard(arguments):
    config_path = os.path.dirname(os.path.realpath(__file__))
    if arguments['new']:
        a = skeletor.Wizard("{}/{}".format(config_path, "skeletor_cli.yaml"))
        a.run()
        a.commit()
