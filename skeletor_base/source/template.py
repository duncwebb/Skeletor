#!/usr/bin/env python
"""{{script_name}}.py

Usage:
  {{script_name}}.py new
  {{script_name}}.py (-h | --help)
  {{script_name}}.py --version

Options:
  -h --help     Show this screen.
  --version     Show version.

"""
import os
from docopt import docopt
from {{package_name}} import {{script_name}}_main as Main

if __name__ == "__main__":
    Main.run_wizard(docopt(__doc__, version='{{installer_desc}} 1.0'))
