import os
from skeletor_core import skeletor

def run_wizard(arguments):
    CONFIG_PATH = os.path.dirname(os.path.realpath(__file__))
    if arguments['new']:
        a = skeletor.Wizard("{}/{}".format(CONFIG_PATH, "{{package_name}}.json"))
        a.run()
        a.commit()