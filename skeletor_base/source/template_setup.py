from setuptools import setup, find_packages

setup(
    name='{{package_name}}',
    version='0.0.1',
    packages=find_packages(),
    package_data={'{{package_name}}': ['source/*', '{{package_name}}.json']},
    scripts=['{{package_name}}/{{script_name}}.py', ],
    url='{{installer_url}}',
    license='{{installer_licence}}',
    author='{{installer_author}}',
    author_email='{{installer_email}}',
    description='{{installer_desc}}',
    long_description=open('README.rst').read(),
    install_requires=[
        "skeletor",
    ],
)
