from abc import ABCMeta, abstractmethod
from collections import defaultdict
from pathlib import Path
from jinja2 import Environment, FileSystemLoader
from urllib.parse import urlparse
import shutil
import os
import json
import yaml
import sys


class Wizard(object):
    """
    The top level wizard object. This loads a JSON wizard description and executes it.
    """
    def __init__(self, inputfile):
        """
        Load a wizard description file.
        """
        if inputfile[-4:] == 'json':
            with open(inputfile, "r") as json_file:
                wizard_data = json.loads(json_file.read())
        elif inputfile[-4:] == 'yaml':
            with open(inputfile, "r") as yaml_file:
                wizard_data = yaml.load(yaml_file.read())
        else:
            raise Exception("Unsupported file type: {}".format(inputfile))

        self._global_ctxt = defaultdict(lambda: None)

        # Store the path to the JSON/YAML beneath which our 'sources' folder will live containing the source files
        self._global_ctxt['_configpath'] = Path(str(inputfile)).parent.as_posix()

        self.root = Document(wizard_data, self._global_ctxt)

    def run(self):
        """
        Traverse the tree of objects and prompt the user for all settings.
        """
        self.root.run()

    def commit(self):
        """
        Traverse the tree of objects and commit the changes to disk.
        """
        commit = YesNo({'id': 'commityn', 'prompt': self.root.commit_prompt, 'default': 'y'}, self._global_ctxt)
        commit.show_prompt()

        if commit.value:
            self.root._commit()
            print(self.root.post_commit_y.format(**self._global_ctxt))
        else:
            print(self.root.post_commit_n.format(**self._global_ctxt))


class WizardNode(object):
    """
    Each parsed object should map to a subclass of wizardnode. This class acts as a node in
    the wizard tree.
    """
    __metaclass__ = ABCMeta

    def __init__(self, nodedict, contextdict):
        """
        Construct an entire subtree at initialisation time from the provided dictionary.
        """
        self._contextdict = contextdict
        self._childnodes = []   # List of of wizardnodes
        self._valuenodes = []   # List of wizardvalues
        self._root_folder = self._contextdict['_configpath']
        self.process_node(nodedict)
        self.process_children()

    def process_node(self, nodedict):
        """
        Create attributes in the current object based on the dictionary passed into this method.
        """
        for k, v in nodedict.items():
            setattr(self, k, v)

    def process_children(self):
        """
        Create child objects.
        """
        if getattr(self, 'children', None) is not None:
            for child in self.children:
                self._childnodes.append(
                    getattr(sys.modules[__name__], child.get("type", "Invalid"))(child, self._contextdict))
            self.children = []
        elif getattr(self, 'values', None) is not None:
            for value in self.values:
                self._valuenodes.append(
                    getattr(sys.modules[__name__], value.get("type", "InvalidValue"))(value, self._contextdict))
            self.values = []

    def run(self):
        """
        Run this node and it's child nodes.
        """
        self.run_step()
        for child in self._childnodes:
            child.run()

    def _commit(self):
        """
        Run this node and it's child nodes.
        """
        self._commit_step()
        for child in self._childnodes:
            child._commit()
        self._post_commit_step()

    def _render_template(self, template_filename):
        """

        """
        return self._template_environment.get_template(template_filename).render(dict(self._contextdict))

    def _create_final_file(self, filename, filesource):
        """
        """
        self._template_environment = Environment(
            autoescape=False,
            loader=FileSystemLoader(os.path.join(self._root_folder, 'source')),
            trim_blocks=False)

        with open(filename, 'w') as f:
            f.write(self._render_template(filesource))

    @abstractmethod
    def run_step(self):
        """
        This is the abstract method that must be implemented in derived classes to implement step specific behaviour.
        """
        pass

    @abstractmethod
    def _commit_step(self):
        """
        Commit changes to disk for the node.
        """
        pass

    def _post_commit_step(self):
        """
        Perform actions after processing child nodes.
        """
        pass


class Document(WizardNode):
    """
    Represents the top level of the JSON document.
    """
    def run_step(self):
        """
        A container for the root node of the JSON (whose type must be 'document').
        """
        pass

    def _commit_step(self):
        """
        Commit changes to disk for the node.
        """
        pass


class Info(WizardNode):
    """
    Represents an info tag. Info tags simply display a string to the user.
    """
    def run_step(self):
        """
        Displays a piece of information to the user.
        """
        print(self.text.format(**self._contextdict))

    def _commit_step(self):
        """
        Commit changes to disk for the node.
        """
        pass


class Directory(WizardNode):
    """
    Represents a directory. This may have a prompt to allow the user to set the directory name. The directory object
    also may have a list of child nodes (including additional nested directory nodes).
    """
    def run_step(self):
        """
        Creates a directory.
        """
        if self.prompt is None:
            self._dirname = self.default.format(**self._contextdict)
        else:
            self._dirname = input(
                "{} [{}]: ".format(self.prompt.format(**self._contextdict), self.default.format(**self._contextdict)))
            if self._dirname == "":
                self._dirname = self.default.format(**self._contextdict)
            else:
                if self._dirname.count(' ') > 0:
                    self._dirname = self._dirname.replace(' ', '_')
                    print("Warning: Spaces in directory name replaced with underscores.")

        if self.id in self._contextdict.keys():
            raise Exception("Error: Directory id {} already defined.".format(self.id))
        self._contextdict[self.id] = self._dirname

    def _commit_step(self):
        """
        Commit changes to disk for the node.
        """
        if os.path.exists(self._dirname):
            print("Error: Project folder exists, exiting.")
            sys.exit(-1)
        else:
            os.mkdir(self._dirname)

            if getattr(self, 'copyfiles', False):
                for filecopy in self.copyfiles:
                    shutil.copy("{}/{}".format(self._root_folder, filecopy), self._dirname)

            if getattr(self, 'templatefiles', False):
                for filetemplate in self.templatefiles:
                    self._create_final_file("{}/{}".format(self._dirname, filetemplate), filetemplate)

            os.chdir(self._dirname)

    def _post_commit_step(self):
        """
        Perform actions after processing child nodes.
        """
        os.chdir("../")


class FileTemplate(WizardNode):
    """
    Represents a file to be processed by Jinja2 using the settings stored in the global context dictionary.
    """
    def run_step(self):
        """
        Creates a file based on a given template in the 'current' folder.
        """

        for node in self._valuenodes:
            node.show_prompt()

            if node.id in self._contextdict.keys():
                raise Exception("Error: Value id {} already defined.".format(node.id))

            self._contextdict[node.id] = node.value

    def _commit_step(self):
        """
        Commit changes to disk for the node.
        """
        self._create_final_file(self.name.format(**self._contextdict), self.source)


class Invalid(WizardNode):
    """
    Represents an invalid node in the tree.
    """
    def run_step(self):
        """
        Created when an object of invalid type is found.
        """
        print("Invalid object found")

    def _commit_step(self):
        """
        Commit changes to disk for the node.
        """
        pass


class WizardValue(object):
    """
    Wizardvalue objects represent values that the user is prompted to enter in response to a question.
    """
    def __init__(self, valuedict, contextdict):
        """

        :param valuedict:
        :return:
        """
        self._contextdict = contextdict
        self.value = None
        for k, v in valuedict.items():
            setattr(self, k, v)

    def show_prompt(self):
        """
        """
        while True:
            self._do_prompt()
            if self._validate():
                break

    @abstractmethod
    def _do_prompt(self):
        """
        """
        pass

    @abstractmethod
    def _validate(self):
        """
        """
        pass


class Text(WizardValue):
    """
    A text type value field.
    """
    def _do_prompt(self):
        """
        """
        self.value = input("{} [{}]: ".format(self.prompt, self.default.format(**self._contextdict)))

    def _validate(self):
        """
        """
        if self.value == "":
            self.value = self.default.format(**self._contextdict)
            return True
        else:
            # Validate
            return True


class Url(WizardValue):
    """
    An URL type value field with validation.
    """
    def _do_prompt(self):
        """
        """
        self.value = input("{} [{}]: ".format(self.prompt, self.default.format(**self._contextdict)))

    def _validate(self):
        """
        """
        if self.value == "":
            self.value = self.default.format(**self._contextdict)
            return True
        else:
            # URL is valid if properly formed and scheme and network location are found
            presult = urlparse(self.value)
            if all((presult.scheme, presult.netloc)):
                return True
            else:
                print("Invalid URL specified")
        return False


class YesNo(WizardValue):
    """
    A boolean (yes or no rather than true ot false) value field.
    """
    def _do_prompt(self):
        """
        """
        self.value = input("{} (y/n) [{}]: ".format(self.prompt, self.default))

    def _validate(self):
        """
        """
        if self.value == "":
            self.value = True if self.default.lower()[:1] == 'y' else False
            return True
        elif self.value.lower() in ['y', 'yes']:
            self.value = True
            return True
        elif self.value.lower() in ['n', 'no']:
            self.value = False
            return True
        else:
            print("Please answer y or n")
        return False


class Number(WizardValue):
    """
    A numeric value entry field (integer) that is validated to a given range.
    """
    def _do_prompt(self):
        """
        """
        self.value = input("{} ({}-{}) [{}]: ".format(self.prompt, self.min, self.max, self.default))

    def _validate(self):
        """
        """
        if self.value == "":
            self.value = self.default
            return True
        elif self.value.isdigit() and self.min < int(self.value) < self.max:
            return True
        else:
            print("Invalid entry")
        return False


class Choice(WizardValue):
    """
    A single choice enumerated value field.
    """
    def _do_prompt(self):
        """
        """
        self.value = input("{} {} [{}]: ".format(self.prompt, str(self.choices), self.default))

    def _validate(self):
        """
        """
        if self.value == "":
            self.value = self.default
            return True
        elif self.value.lower() in [x.lower() for x in self.choices]:
            return True
        else:
            print("Invalid entry")
        return False


class InvalidValue(WizardValue):
    """
    Represents an invalid value field in the JSON document.
    """

    def _validate(self):
        """
        """
        pass

    def _do_prompt(self):
        """
        """
        pass
